Source: rdkit
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>,
           Andrius Merkys <merkys@debian.org>
Build-Depends: architecture-is-64-bit,
               architecture-is-little-endian,
               bison,
               catch2,
               cmake,
               debhelper-compat (= 13),
               dh-python,
               doxygen,
               flex,
               imagemagick,
               latexmk,
               libboost-dev,
               libboost-iostreams-dev,
               libboost-numpy-dev,
               libboost-program-options-dev,
               libboost-python-dev,
               libboost-regex-dev,
               libboost-system-dev,
               libboost-thread-dev,
               libcairo2-dev,
               libcoordgen-dev,
               libeigen3-dev,
               libfreetype-dev,
               libinchi-dev (>= 1.07.1+dfsg-2),
               libmaeparser-dev,
               librsvg2-bin,
               libsqlite3-dev,
               pandoc,
               postgresql-server-dev-all,
               python3-dev,
               python3-myst-parser,
               python3-numpy,
               python3-pandas,
               python3-pil | python3-imaging,
               python3-pytest,
               python3-recommonmark,
               python3-sphinx,
               python3-sqlalchemy,
               rapidjson-dev,
               tex-gyre,
               texlive-fonts-recommended,
               texlive-latex-base,
               texlive-latex-extra,
               texlive-latex-recommended
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debichem-team/rdkit
Vcs-Git: https://salsa.debian.org/debichem-team/rdkit.git
Homepage: http://www.rdkit.org

Package: python3-rdkit
Section: python
Architecture: any
Depends: fonts-freefont-ttf,
         rdkit-data,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Suggests: rdkit-doc
Provides: ${python3:Provides}
Description: Collection of cheminformatics and machine-learning software
 RDKit is a Python/C++ based cheminformatics and machine-learning software
 environment.  Features Include:
 .
  * Chemical reaction handling and transforms
  * Substructure searching with SMARTS
  * Canonical SMILES
  * Molecule-molecule alignment
  * Large number of molecular descriptors, including topological,
    compositional, EState, SlogP/SMR, VSA and Feature-map vectors
  * Fragmentation using RECAP rules
  * 2D coordinate generation and depiction, including constrained depiction
  * 3D coordinate generation using geometry embedding
  * UFF and MMFF94 forcefields
  * Chirality support, including calculation of (R/S) stereochemistry codes
  * 2D pharmacophore searching
  * Fingerprinting, including Daylight-like, atom pairs, topological
    torsions, Morgan algorithm and MACCS keys
  * Calculation of shape similarity
  * Multi-molecule maximum common substructure
  * Machine-learning via clustering and information theory algorithms
  * Gasteiger-Marsili partial charge calculation
 .
 File formats RDKit supports include MDL Mol, PDB, SDF, TDT, SMILES and RDKit
 binary format.

Package: rdkit-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}, libjs-jquery, libjs-underscore
Description: Collection of cheminformatics and machine-learning software (documentation)
 RDKit is a Python/C++ based cheminformatics and machine-learning software
 environment.
 .
 This package contains the documentation.

Package: rdkit-data
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Collection of cheminformatics and machine-learning software (data files)
 RDKit is a Python/C++ based cheminformatics and machine-learning software
 environment.
 .
 This package contains data files.

Package: librdkit1t64
Provides: ${t64:Provides}
Replaces: librdkit1
Breaks: librdkit1 (<< ${source:Version})
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Collection of cheminformatics and machine-learning software (shared libraries)
 RDKit is a Python/C++ based cheminformatics and machine-learning software
 environment.  Features Include:
 .
  * Chemical reaction handling and transforms
  * Substructure searching with SMARTS
  * Canonical SMILES
  * Molecule-molecule alignment
  * Large number of molecular descriptors, including topological,
    compositional, EState, SlogP/SMR, VSA and Feature-map vectors
  * Fragmentation using RECAP rules
  * 2D coordinate generation and depiction, including constrained depiction
  * 3D coordinate generation using geometry embedding
  * UFF and MMFF94 forcefields
  * Chirality support, including calculation of (R/S) stereochemistry codes
  * 2D pharmacophore searching
  * Fingerprinting, including Daylight-like, atom pairs, topological
    torsions, Morgan algorithm and MACCS keys
  * Calculation of shape similarity
  * Multi-molecule maximum common substructure
  * Machine-learning via clustering and information theory algorithms
  * Gasteiger-Marsili partial charge calculation
 .
 File formats RDKit supports include MDL Mol, PDB, SDF, TDT, SMILES and RDKit
 binary format.
 .
 This package contains the shared libraries.

Package: librdkit-dev
Section: libdevel
Architecture: any
Depends: librdkit1t64 (= ${binary:Version}), ${misc:Depends}, ${shlibs:Depends}
Description: Collection of cheminformatics and machine-learning software (development files)
 RDKit is a Python/C++ based cheminformatics and machine-learning software
 environment.  Features Include:
 .
  * Chemical reaction handling and transforms
  * Substructure searching with SMARTS
  * Canonical SMILES
  * Molecule-molecule alignment
  * Large number of molecular descriptors, including topological,
    compositional, EState, SlogP/SMR, VSA and Feature-map vectors
  * Fragmentation using RECAP rules
  * 2D coordinate generation and depiction, including constrained depiction
  * 3D coordinate generation using geometry embedding
  * UFF and MMFF94 forcefields
  * Chirality support, including calculation of (R/S) stereochemistry codes
  * 2D pharmacophore searching
  * Fingerprinting, including Daylight-like, atom pairs, topological
    torsions, Morgan algorithm and MACCS keys
  * Calculation of shape similarity
  * Multi-molecule maximum common substructure
  * Machine-learning via clustering and information theory algorithms
  * Gasteiger-Marsili partial charge calculation
 .
 File formats RDKit supports include MDL Mol, PDB, SDF, TDT, SMILES and RDKit
 binary format.
 .
 This package contains the header files.

Package: postgresql-17-rdkit
Architecture: any
Section: database
Depends: ${misc:Depends}, ${shlibs:Depends}, postgresql-17
Description: Cheminformatics and machine-learning software (PostgreSQL Cartridge)
 RDKit is a Python/C++ based cheminformatics and machine-learning software
 environment.  Features Include:
 .
  * Chemical reaction handling and transforms
  * Substructure searching with SMARTS
  * Canonical SMILES
  * Molecule-molecule alignment
  * Large number of molecular descriptors, including topological,
    compositional, EState, SlogP/SMR, VSA and Feature-map vectors
  * Fragmentation using RECAP rules
  * 2D coordinate generation and depiction, including constrained depiction
  * 3D coordinate generation using geometry embedding
  * UFF and MMFF94 forcefields
  * Chirality support, including calculation of (R/S) stereochemistry codes
  * 2D pharmacophore searching
  * Fingerprinting, including Daylight-like, atom pairs, topological
    torsions, Morgan algorithm and MACCS keys
  * Calculation of shape similarity
  * Multi-molecule maximum common substructure
  * Machine-learning via clustering and information theory algorithms
  * Gasteiger-Marsili partial charge calculation
 .
 File formats RDKit supports include MDL Mol, PDB, SDF, TDT, SMILES and RDKit
 binary format.
 .
 This package contains the PostgreSQL extension.
